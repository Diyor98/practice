from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import (
    IsAdminUser,
    AllowAny
)

from stack.models import Answer, Question
from stack.serializers import (
    QuestionSerializer,
    AnswerSerializer
)


class QuestionModelViewSet(ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [IsAdminUser, ]

    def get_permissions(self):
        if self.action in ('create', 'update', 'delete'):
            permission_classes = [IsAdminUser, ]
        else:
            permission_classes = [AllowAny, ]

        return [permission() for permission in permission_classes]


class AnswerModelViewSet(ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [IsAdminUser, ]

    def get_permissions(self):
        if self.action in ('create', 'update', 'delete'):
            permission_classes = [IsAdminUser, ]
        else:
            permission_classes = [AllowAny]

        return [permission() for permission in permission_classes]
