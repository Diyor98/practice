from django.db import models

class User(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Question(models.Model):
    question = models.TextField()
    upvotes = models.IntegerField(default= 0)
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return self.question

class Answer(models.Model):
    answer = models.TextField()
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    user = models.ForeignKey(User,on_delete = models.CASCADE)

    def __str__(self):
        return self.answer
    class Meta:
        unique_together = ('user', 'question')