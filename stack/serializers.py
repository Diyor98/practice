from stack.model import Question, Answer
from rest_framework.serializers import ModelSerializer

class QuestionSerializer(ModelSerializer):
    model = Question
    fields = "__all__"

class AnswerSerializer(ModelSerializer):
    model = Answer
    fields = "__all__"